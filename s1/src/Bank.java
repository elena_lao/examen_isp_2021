import java.util.ArrayList;

public class Bank {
    private ArrayList<Account> accounts;
    private ArrayList<User> users;

    public Bank() {
        accounts = new ArrayList<>();
        users = new ArrayList<>();
    }

    public void addAccount(User user) {
        this.users.add(user);
        Account nw = new Account();
        user.addAccount(nw);
        this.accounts.add(nw);
    }
}

class Account {
    private Card card;

    public Account() {
    }

    public void setCard() {
        Card nw = new Card();
        this.card = nw;
    }
}

class Card {

    public Card() {

    }

}

class User {
    private ArrayList<Account> accounts;

    public User() {
        this.accounts = new ArrayList<>();
    }

    public void addAccount(Account nw) {
        this.accounts.add(nw);
    }
}

class Transaction {
    private int amount;
    private Account ac1;
    private Account ac2;

    public Transaction(int amount, Account ac1, Account ac2) {
        this.amount = amount;
        this.ac1 = ac1;
        this.ac2 = ac2;
    }
}