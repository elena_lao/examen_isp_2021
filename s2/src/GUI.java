import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    JTextField tField;
    JTextArea tArea;
    JButton button;

    GUI() {
        setTitle("My GUI application");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300,300);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 100;
        int height = 40;

        tField = new JTextField();
        tField.setBounds(20, 30, width, height);

        tArea = new JTextArea();
        tArea.setBounds(20,90, 260, height*2);

        button = new JButton("Press to append");
        button.setBounds(40,200,  width*2, height);
        button.addActionListener(new Append());

        add(tField);
        add(tArea);
        add(button);
    }

    class Append implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String get = GUI.this.tField.getText();

            GUI.this.tField.setText("");

            GUI.this.tArea.append(get + "\n");
        }
    }


    public static void main(String[] args) {
        new GUI();
    }
}

