public class Test {
    private int x;
    private int y;

    public Test (int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int add() {
        return x+y;
    }

    public int getX() {
        return x;
    }
}
